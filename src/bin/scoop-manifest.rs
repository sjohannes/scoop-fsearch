//! Displays Scoop package manifest.

use std::io::Write;

fn main_(args: impl IntoIterator<Item = std::ffi::OsString>) -> Result<(), String> {
    let mut args = args.into_iter();
    let mut package_filename = {
        let arg = args.nth(1);
        match arg {
            Some(arg) if args.next().is_none() => arg,
            _ => {
                return Err(concat!(
                    concat!(env!("CARGO_BIN_NAME"), " ", env!("CARGO_PKG_VERSION"), "\n"),
                    concat!("Syntax: ", env!("CARGO_BIN_NAME"), " PACKAGE\n"),
                )
                .to_owned());
            }
        }
    };
    package_filename.push(".json");

    let scoop_dir = scoop_fsearch::get_scoop_dir(|k| std::env::var_os(k))?;
    let bucket_dirs = scoop_dir
        .join("buckets")
        .read_dir()
        .map_err(|_| "Error reading Scoop buckets directory\n")?
        .filter_map(std::result::Result::ok)
        .map(|d| d.path());
    let matched_packages_iter = bucket_dirs.filter_map(|bucket_dir| {
        let mut path = bucket_dir.join("bucket").join(&package_filename);
        let mut file = std::fs::File::open(&path);
        if matches!(&file, Err(e) if e.kind() == std::io::ErrorKind::NotFound) {
            path = bucket_dir.join(&package_filename);
            file = std::fs::File::open(&path);
            if matches!(&file, Err(e) if e.kind() == std::io::ErrorKind::NotFound) {
                return None;
            }
        }
        Some((path, file))
    });

    let mut found = false;
    let mut read_error = false;
    for (path, file) in matched_packages_iter {
        found = true;
        let path = path.to_string_lossy().to_string();
        let stdout = std::io::stdout();
        let mut stdout = stdout.lock();
        let _ = stdout.write_all((path + "\n").as_bytes());
        match file {
            Ok(mut file) => {
                let _ = std::io::copy(&mut file, &mut stdout);
                let _ = stdout.write_all(b"\n");
            }
            Err(err) => {
                read_error = true;
                let _ = stdout.write_all(err.to_string().as_bytes());
                let _ = stdout.write_all(b"\n");
            }
        };
    }

    if !found {
        Err("Package not found\n".to_owned())
    } else if read_error {
        Err("Error reading one or more manifest files\n".to_owned())
    } else {
        Ok(())
    }
}

fn main() {
    if let Err(err) = main_(std::env::args_os()) {
        debug_assert!(err.ends_with('\n'));
        let _ = std::io::stderr().write_all(err.as_bytes());
        std::process::exit(1);
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn nonexistent() {
        assert!(main_(["".into(), "nonexistent".into()]).is_err());
    }

    #[test]
    fn syntax() {
        let scoop_dir = [env!("CARGO_MANIFEST_DIR"), "data", "test-home", "scoop"]
            .iter()
            .collect::<std::path::PathBuf>();
        std::env::set_var("SCOOP", scoop_dir);
        assert!(main_(["".into()]).is_err());
        assert!(main_(["".into(), "basic".into()]).is_ok());
        assert!(main_(["".into(), "basic".into(), "basic".into()]).is_err());
    }
}
