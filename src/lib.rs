//! `scoop search` alternative.

#![warn(missing_docs)]

#[cfg(test)]
mod tests;
#[cfg(windows)]
mod windows;

use serde_json::Value;

// Number of worker threads if an optimal one cannot be determined.
const DEFAULT_THREADS: usize = 2;
/// Maximum number of worker threads.
const MAX_THREADS: usize = 12;

/// Marker for where/how a match is found.
///
/// This enum is specifically written to also be usable as a line prefix
/// character to make the lines sort nicely (exact matches first, and so on).
#[derive(Clone, Copy)]
#[repr(u8)]
enum MatchMarker {
    /// Exact package name match: `!`.
    Exact = b'!',
    /// Package name match: `"`.
    Name,
    /// Description match: `#`.
    Desc,
    /// Executable filename match: `$`.
    Bin,
}

/// Terminal escape strings to format the output text.
#[derive(Clone)]
struct Color {
    /// Formatting reset.
    default: &'static str,
    /// Highlighting formats, corresponding to [`MatchMarker`] members.
    highlight: [&'static str; 4],
    /// Underline start.
    underline: &'static str,
    /// Underline end.
    underline_off: &'static str,
}
impl Color {
    /// Gets the highlighting format that matches a marker.
    fn highlight(&self, marker: MatchMarker) -> &'static str {
        self.highlight[marker as usize - MatchMarker::Exact as usize]
    }
}
/// Normal color strings.
const COLOR: Color = Color {
    // https://man7.org/linux/man-pages/man4/console_codes.4.html
    // (search for SGR)
    default: "\x1B[m",
    highlight: ["\x1B[92m", "\x1B[93m", "\x1B[96m", "\x1B[97m"],
    underline: "\x1B[4m",
    underline_off: "\x1B[24m",
};
/// Empty color strings.
const NO_COLOR: Color = Color {
    default: "",
    highlight: ["", "", "", ""],
    underline: "",
    underline_off: "",
};

/// Reads a file and parses it as JSON.
fn parse_json_file(path: impl AsRef<std::path::Path>) -> Option<Value> {
    // We expect the file to be small, so slurping it first makes this faster.
    // In particular, do not use serde_json::from_reader here; see
    // https://github.com/serde-rs/json/issues/160
    let buf = std::fs::read(path).ok()?;
    serde_json::from_slice(&buf).ok()
}

/// Checks whether a JSON node contains a "bin" that matches a keyword.
///
/// `keyword` must have already gone through [`str::to_uppercase`].
///
/// The value of "bin" is a shim definition, which can either be:
/// * the executable path, or
/// * an array, where each item is either:
///   * an executable path,
///   * an executable path inside a single-element array (not recommended; this
///     form is not used in the core and extras buckets), or
///   * a renamed shim definition, which is an array containing:
///     * the original executable path (not searched, unlike `scoop search`),
///     * the file stem of the shim, and
///     * optionally, arguments to the executable (not searched).
///
/// In TypeScript, the type would be
/// ```typescript
/// | string
/// | (
///   | string
///   | [string]
///   | [string, string, ...string[]]
///   )[]
/// ```
fn match_bin_inner(node: &Value, keyword: &str) -> bool {
    debug_assert_eq!(keyword.to_uppercase(), keyword);
    fn get_bin_name(path: &str) -> Option<String> {
        Some(
            std::path::PathBuf::from(path)
                .file_stem()?
                .to_str()
                .unwrap() // This was originally a string
                .to_uppercase(),
        )
    }
    fn bin_match(keyword: &str, bin: &Value) -> bool {
        bin.as_str()
            .and_then(get_bin_name)
            .map(|bin| bin.contains(keyword))
            .unwrap_or(false)
    }
    if let Some(bin) = node.get("bin") {
        if bin_match(keyword, bin) {
            return true;
        }
        if let Some(bin) = bin.as_array() {
            for bin in bin {
                if bin_match(keyword, bin) {
                    return true;
                }
                // If this is an array, first value is shim target, (optional)
                // second value is shim name.
                // TODO: This code may not be quite right; shim name should not
                // be stemmed.
                if let Some([_, bin, ..] | [bin]) = bin.as_array().map(Vec::as_slice) {
                    if bin_match(keyword, bin) {
                        return true;
                    }
                }
            }
        }
    }
    false
}

/// Checks whether any "bin" in a package JSON matches a keyword.
fn match_bin(pkg: &Value, keyword: &str) -> bool {
    if match_bin_inner(pkg, keyword) {
        return true;
    }
    pkg.get("architecture")
        .and_then(Value::as_object)
        .is_some_and(|arch| {
            arch.values()
                .any(|arch_val| match_bin_inner(arch_val, keyword))
        })
}

/// Underlines (case-insensitive) occurences of `needle` within `haystack`.
///
/// `needle` must have already gone through [`str::to_uppercase`].
///
/// Returns `None` if there is no match.
/// Otherwise, returns `Some((underlined_string, is_exact_match))`.
fn find_and_underline(haystack: &str, needle: &str, color: &Color) -> Option<(String, bool)> {
    debug_assert_eq!(needle.to_uppercase(), needle);
    if needle.is_empty() {
        return Some((haystack.to_owned(), false));
    }
    let haystack_upper = haystack.to_uppercase();

    if haystack_upper.len() != haystack.len() {
        // Too hard, so don't bother underlining
        if haystack_upper.contains(needle) {
            return Some((haystack.to_owned(), haystack_upper.len() == needle.len()));
        } else {
            return None;
        };
    }

    // Allocate result assuming that usually there is at most one match
    let mut result =
        String::with_capacity(haystack.len() + color.underline.len() + color.underline_off.len());
    let mut first: usize = 0;
    let mut last: usize = 0;
    for (i, _) in haystack_upper.match_indices(needle) {
        result.push_str(&haystack[last..i]);
        result.push_str(color.underline);
        result.push_str(&haystack[i..i + needle.len()]);
        result.push_str(color.underline_off);
        last = i + needle.len();
        if first == 0 {
            first = i;
        }
    }
    if last == 0 {
        return None;
    }
    result.push_str(&haystack[last..]);
    Some((result, first == 0 && last == haystack.len()))
}

/// Finds match in a package JSON object.
///
/// Returns `Some((marker, package_name, version, description))` if found,
/// otherwise `None`.
fn match_pkg<'a>(
    pkg: Option<&'a Value>,
    pkg_name: String,
    keyword: &str,
    color: &Color,
) -> Option<(MatchMarker, String, Option<&'a str>, Option<String>)> {
    let (ver, desc) = pkg
        .map(|pkg| {
            (
                pkg.get("version").and_then(Value::as_str),
                pkg.get("description").and_then(Value::as_str),
            )
        })
        .unwrap_or_default();
    if keyword.is_empty() {
        return Some((
            MatchMarker::Name,
            pkg_name,
            ver,
            desc.map(ToOwned::to_owned),
        ));
    }
    if let Some((pkg_name, exact)) = find_and_underline(&pkg_name, keyword, color) {
        let marker = if exact {
            MatchMarker::Exact
        } else {
            MatchMarker::Name
        };
        let desc = desc.map(|desc| match find_and_underline(desc, keyword, color) {
            Some((desc, _)) => desc,
            None => desc.to_owned(),
        });
        Some((marker, pkg_name, ver, desc))
    } else {
        if let Some(desc) = desc.as_ref() {
            if let Some((desc, _)) = find_and_underline(desc, keyword, color) {
                return Some((MatchMarker::Desc, pkg_name, ver, Some(desc)));
            }
        }
        if let Some(pkg) = pkg {
            if match_bin(pkg, keyword) {
                return Some((MatchMarker::Bin, pkg_name, ver, desc.map(ToOwned::to_owned)));
            }
        }
        None
    }
}

/// Reads, parses, and finds match in a file.
fn process_file(
    path: &std::path::Path,
    pkg_name: String,
    keyword: &str,
    color: &Color,
    out_buf: Option<&std::sync::Arc<std::sync::Mutex<String>>>,
) {
    let pkg = parse_json_file(path);
    let Some((marker, pkg_name, ver, desc)) = match_pkg(pkg.as_ref(), pkg_name, keyword, color)
    else {
        return;
    };

    // Make sure the output is parsable
    if pkg_name.bytes().any(|c| matches!(c, b'\n' | b' ')) {
        return;
    }
    let ver = ver
        .and_then(|s| s.bytes().all(|c| !matches!(c, b'\n' | b']')).then_some(s))
        .unwrap_or_default();
    let desc = desc.map_or_else(
        || "".to_owned(),
        |s| s.chars().map(|c| if c == '\n' { ' ' } else { c }).collect(),
    );

    let line = format!(
        "{marker} {highlight}{pkg_name}{default} [{ver}]: {desc}\n",
        marker = marker as u8 as char,
        highlight = color.highlight(marker),
        default = color.default,
    );
    if let Some(out_buf) = out_buf {
        if let Ok(mut s) = out_buf.lock() {
            s.push_str(&line);
        }
    } else {
        use std::io::Write;
        let _ = std::io::stdout().write_all(line.as_bytes());
    }
}

/// Enumerates files in a bucket directory and runs [`process_file`] on each.
///
/// If the directory contains a readable subdirectory named "bucket", it is used
/// as the bucket directory instead of the original.
///
/// IO errors on individual files are ignored.
/// The only time this function returns an error is when the initial directory
/// read fails.
fn process_bucket(
    pool: Option<&threadpool::ThreadPool>,
    pkg_dir: &std::path::Path,
    keyword: &str,
    color: &Color,
    out_buf: Option<&std::sync::Arc<std::sync::Mutex<String>>>,
) -> std::io::Result<()> {
    let pkg_files = pkg_dir
        .join("bucket")
        .read_dir()
        .or_else(|_| pkg_dir.read_dir())?;
    let pkg_files = pkg_files
        .filter_map(Result::ok)
        .map(|f| f.path())
        .filter(|f| f.extension() == Some(std::ffi::OsStr::new("json")));
    for pkg_file in pkg_files {
        // The unwrap is ensured by the extension filter
        let pkg_name = pkg_file.file_stem().unwrap();
        let pkg_name = match pkg_name.to_str() {
            Some(s) => s.to_owned(),
            None => continue,
        };
        if let Some(pool) = pool {
            let keyword = keyword.to_owned();
            let color = color.to_owned();
            let out_buf = out_buf.map(std::clone::Clone::clone);
            pool.execute(move || {
                process_file(&pkg_file, pkg_name, &keyword, &color, out_buf.as_ref());
            });
        } else {
            process_file(&pkg_file, pkg_name, keyword, color, out_buf);
        }
    }
    Ok(())
}

/// Gets the Scoop directory.
pub fn get_scoop_dir(
    get_env: fn(&str) -> Option<std::ffi::OsString>,
) -> Result<std::path::PathBuf, String> {
    let get_env = |k| get_env(k).filter(|v| !v.is_empty());
    if let Some(scoop_dir) = get_env("SCOOP") {
        return Ok(scoop_dir.into());
    }
    if let Some(home_dir) = get_env("USERPROFILE") {
        let home_dir = std::path::PathBuf::from(home_dir);
        if let Some(Value::Object(config)) =
            parse_json_file(home_dir.join(".config/scoop/config.json"))
        {
            // NOTE: Scoop 0.3.0 (2022-10-10) changed this from rootPath to
            // root_path
            if let Some(Value::String(scoop_dir)) =
                config.get("root_path").or_else(|| config.get("rootPath"))
            {
                return Ok(scoop_dir.into());
            }
        }
        return Ok(home_dir.join("scoop"));
    }
    Err("The SCOOP or USERPROFILE environment variable must be set and not empty\n".to_owned())
}

/// Runs the program.
pub fn main(
    mut args: impl Iterator<Item = std::ffi::OsString>,
    get_env: fn(&str) -> Option<std::ffi::OsString>,
    out_buf: Option<&std::sync::Arc<std::sync::Mutex<String>>>,
) -> Result<(), String> {
    // https://no-color.org/
    let color = if get_env("NO_COLOR").is_none() {
        // NOTE: enable_vt fails if stdout is not a terminal, but it doesn't
        // matter; we still need to output colors
        #[cfg(windows)]
        let _ = self::windows::enable_vt();
        &COLOR
    } else {
        &NO_COLOR
    };

    let keyword = {
        let arg = args.nth(1);
        match arg {
            Some(arg) if args.next().is_none() => arg,
            _ => {
                return Err(format!(
                    r#"{pkg_name} {pkg_version}

Syntax: {pkg_name} STRING

Output legend: (if you sort the output, they will be in this order)
  ! {hl_exact}exact package name match{default}
  " {hl_name}package name match{default}
  # {hl_desc}description match{default}
  $ {hl_bin}executable filename match{default}

Run with NO_COLOR set if you don't want colors.
"#,
                    pkg_name = env!("CARGO_PKG_NAME"),
                    pkg_version = env!("CARGO_PKG_VERSION"),
                    hl_exact = color.highlight(MatchMarker::Exact),
                    hl_name = color.highlight(MatchMarker::Name),
                    hl_desc = color.highlight(MatchMarker::Desc),
                    hl_bin = color.highlight(MatchMarker::Bin),
                    default = color.default,
                ));
            }
        }
    };
    let keyword = keyword
        .to_str()
        .ok_or("Search string must be valid Unicode\n")?
        .to_uppercase();

    let scoop_dir = get_scoop_dir(get_env)?;
    let buckets_dir = scoop_dir.join("buckets");

    #[allow(clippy::assertions_on_constants)]
    const _: () = debug_assert!(DEFAULT_THREADS <= MAX_THREADS);
    let n_threads =
        std::thread::available_parallelism().map_or(DEFAULT_THREADS, |n| n.get().min(MAX_THREADS));

    let pool = threadpool::ThreadPool::with_name("worker".to_owned(), n_threads);

    let pkg_dirs = buckets_dir
        .read_dir()
        .map_err(|_| "Error reading Scoop buckets directory\n")?
        .filter_map(Result::ok)
        .map(|d| d.path());
    for pkg_dir in pkg_dirs {
        let pool1 = pool.clone();
        let keyword = keyword.clone();
        let out_buf = out_buf.map(std::clone::Clone::clone);
        pool.execute(move || {
            let _ = process_bucket(Some(&pool1), &pkg_dir, &keyword, color, out_buf.as_ref());
        });
    }

    pool.join();
    Ok(())
}
