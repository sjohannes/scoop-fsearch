mod util {
    use std::{
        path::{Path, PathBuf},
        sync::{Arc, Mutex},
    };

    pub struct EasyBuf(pub Arc<Mutex<String>>);
    impl EasyBuf {
        pub fn new() -> Self {
            Self(Arc::new(Mutex::new(String::new())))
        }

        pub fn unwrap(self) -> String {
            Arc::try_unwrap(self.0).unwrap().into_inner().unwrap()
        }
    }

    pub fn join(elements: impl IntoIterator<Item = impl AsRef<Path>>) -> PathBuf {
        elements.into_iter().collect()
    }

    pub fn home() -> PathBuf {
        join(&[env!("CARGO_MANIFEST_DIR"), "data", "test-home"])
    }

    pub fn repo1() -> PathBuf {
        home().join(join(&["scoop", "buckets", "repo1"]))
    }

    pub fn bucket1() -> PathBuf {
        repo1().join("bucket")
    }

    pub fn config_home() -> PathBuf {
        join(&[env!("CARGO_MANIFEST_DIR"), "data", "test-config"])
    }

    pub fn easy_process_bucket(path: &std::path::Path, keyword: &str) -> Vec<String> {
        let buf = EasyBuf::new();
        crate::process_bucket(
            None,
            path,
            &keyword.to_uppercase(),
            &crate::NO_COLOR,
            Some(&buf.0),
        )
        .unwrap();
        let mut lines: Vec<_> = buf
            .unwrap()
            .split_terminator('\n')
            .map(ToOwned::to_owned)
            .collect();
        lines.sort();
        lines
    }

    pub fn easy_process_file(path: &std::path::Path, keyword: &str) -> String {
        let buf = EasyBuf::new();
        crate::process_file(
            path,
            path.file_stem().unwrap().to_str().unwrap().to_owned(),
            &keyword.to_uppercase(),
            &crate::NO_COLOR,
            Some(&buf.0),
        );
        buf.unwrap()
    }
}

use self::util::*;

#[test]
fn scoop_dir() {
    let ret =
        crate::get_scoop_dir(|k| (k == "USERPROFILE").then(|| config_home().into_os_string()));
    assert_eq!(Ok(std::path::Path::new(".").to_owned()), ret);
    let ret = crate::get_scoop_dir(|k| (k == "USERPROFILE").then(|| home().into_os_string()));
    assert_eq!(Ok(home().join("scoop")), ret);
}

#[test]
fn bucket() {
    let ret = easy_process_bucket(&bucket1(), "basic");
    assert_eq!(vec!["! basic [basic]: basic"], ret);
    let ret = easy_process_bucket(&repo1(), "basic");
    assert_eq!(vec!["! basic [basic]: basic"], ret);
    let ret = easy_process_bucket(&repo1(), "outside");
    assert_eq!(Vec::<String>::new(), ret);
}

#[test]
fn bin_dir_and_ext() {
    let ret = easy_process_file(&bucket1().join("binstr.json"), "bindir");
    assert_eq!("", ret);
    let ret = easy_process_file(&bucket1().join("binstr.json"), "exe");
    assert_eq!("", ret);
}

#[test]
fn file_arch_bin() {
    let ret = easy_process_file(&bucket1().join("archbin.json"), "arch64bin-");
    assert_eq!("$ archbin []: \n", ret);
    let ret = easy_process_file(&bucket1().join("archbin.json"), "arch32bin-");
    assert_eq!("$ archbin []: \n", ret);
}

#[test]
fn file_bin_array() {
    let ret = easy_process_file(&bucket1().join("binarr.json"), "bin-");
    assert_eq!("$ binarr []: \n", ret);
    let ret = easy_process_file(&bucket1().join("binarr.json"), "bin-1");
    assert_eq!("$ binarr []: \n", ret);
    let ret = easy_process_file(&bucket1().join("binarr.json"), "bin-2");
    assert_eq!("$ binarr []: \n", ret);
    let ret = easy_process_file(&bucket1().join("binarr.json"), "bin-3");
    assert_eq!("$ binarr []: \n", ret);
    let ret = easy_process_file(&bucket1().join("binarr.json"), "bin-4");
    assert_eq!("$ binarr []: \n", ret);
    let ret = easy_process_file(&bucket1().join("binarr.json"), "bin-5");
    assert_eq!("$ binarr []: \n", ret);
    // Only check the second item
    let ret = easy_process_file(&bucket1().join("binarr.json"), "bin-2-1");
    assert_eq!("", ret);
    let ret = easy_process_file(&bucket1().join("binarr.json"), "bin-2-2");
    assert_eq!("$ binarr []: \n", ret);
    let ret = easy_process_file(&bucket1().join("binarr.json"), "bin-3-1");
    assert_eq!("", ret);
    let ret = easy_process_file(&bucket1().join("binarr.json"), "bin-3-2");
    assert_eq!("$ binarr []: \n", ret);
    let ret = easy_process_file(&bucket1().join("binarr.json"), "bin-3-3");
    assert_eq!("", ret);
}

#[test]
fn file_bin_string() {
    let ret = easy_process_file(&bucket1().join("binstr.json"), "bin-");
    assert_eq!("$ binstr []: \n", ret);
    let ret = easy_process_file(&bucket1().join("descbin.json"), "bin-");
    assert_eq!("$ descbin []: desc-1\n", ret);
}

#[test]
fn file_invalid() {
    let ret = easy_process_file(&bucket1().join("empty.json"), "...");
    assert_eq!("", ret);
    let ret = easy_process_file(&bucket1().join("invalid.json"), "...");
    assert_eq!("", ret);
}

#[test]
fn file_name() {
    let ret = easy_process_file(&bucket1().join("empty.json"), "empt");
    assert_eq!("\" empty []: \n", ret);
    let ret = easy_process_file(&bucket1().join("empty.json"), "empty");
    assert_eq!("! empty []: \n", ret);
    // let ret = easy_process_file(&bucket1().join("ver.json"), "ver");
    // assert_eq!("! ver [ver-1]: \n", ret);
    let ret = easy_process_file(&bucket1().join("desc.json"), "desc");
    assert_eq!("! desc []: desc-1\n", ret);
    let ret = easy_process_file(&bucket1().join("binstr.json"), "binstr");
    assert_eq!("! binstr []: \n", ret);
    let ret = easy_process_file(&bucket1().join("invalid.json"), "invalid");
    assert_eq!("! invalid []: \n", ret);
}

#[test]
fn main() {
    let buf = EasyBuf::new();
    let ret = crate::main(
        vec!["".into(), "basic".into()].into_iter(),
        |k| (k == "NO_COLOR").then(|| "".into()),
        Some(&buf.0),
    );
    assert!(ret.is_err(), "{:?}", ret);

    let buf = EasyBuf::new();
    let ret = crate::main(
        vec!["".into(), "basic".into()].into_iter(),
        |k| match k {
            "NO_COLOR" => Some("".into()),
            "USERPROFILE" => Some(home().join("nonexistent").into_os_string()),
            _ => None,
        },
        Some(&buf.0),
    );
    assert!(ret.is_err(), "{:?}", ret);

    let buf = EasyBuf::new();
    let ret = crate::main(
        vec!["".into(), "basic".into()].into_iter(),
        |k| match k {
            "NO_COLOR" => Some("".into()),
            "USERPROFILE" => Some(home().into_os_string()),
            _ => None,
        },
        Some(&buf.0),
    );
    assert!(ret.is_ok(), "{:?}", ret);
    let buf = buf.unwrap();
    let mut lines: Vec<_> = buf.split_terminator('\n').collect();
    lines.sort();
    assert_eq!(
        vec!["! basic [basic]: basic", "\" basic2 [basic2]: basic2"],
        lines,
    );

    let buf = EasyBuf::new();
    let ret = crate::main(
        vec!["".into(), "basic".into()].into_iter(),
        |k| match k {
            "NO_COLOR" => Some("".into()),
            "SCOOP" => Some(home().join("scoop").into_os_string()),
            "USERPROFILE" => Some(home().join("nonexistent").into_os_string()),
            _ => None,
        },
        Some(&buf.0),
    );
    assert!(ret.is_ok(), "{:?}", ret);
    let buf = buf.unwrap();
    let mut lines: Vec<_> = buf.split_terminator('\n').collect();
    lines.sort();
    assert_eq!(
        vec!["! basic [basic]: basic", "\" basic2 [basic2]: basic2"],
        lines,
    );
}
