//! Windows-specific functionalities.

/// Raw Windows API bindings.
#[allow(clippy::upper_case_acronyms)]
mod c {
    use std::os::raw::{c_int, c_ulong};
    pub use std::os::windows::raw::HANDLE;

    pub type BOOL = c_int;
    pub type DWORD = c_ulong;
    pub type LPDWORD = *mut DWORD;

    extern "system" {
        pub fn GetConsoleMode(hConsoleHandle: HANDLE, lpMode: LPDWORD) -> BOOL;
        pub fn SetConsoleMode(hConsoleHandle: HANDLE, dwMode: DWORD) -> BOOL;
    }

    pub const ENABLE_VIRTUAL_TERMINAL_PROCESSING: DWORD = 0x0004;
}

use self::c::*;

/// Enables [console virtual terminal sequences][vt] (a.k.a. ANSI escape
/// sequences) support on stdout.
///
/// [vt]: https://docs.microsoft.com/en-us/windows/console/console-virtual-terminal-sequences
pub fn enable_vt() -> Result<(), ()> {
    use std::os::windows::io::AsRawHandle;
    let h_stdout = std::io::stdout().as_raw_handle();
    let mut mode: DWORD = 0;
    let mode_mut = &mut mode;
    if unsafe { GetConsoleMode(h_stdout, mode_mut) } == 0 {
        return Err(());
    }
    let mode = mode | ENABLE_VIRTUAL_TERMINAL_PROCESSING;
    if unsafe { SetConsoleMode(h_stdout, mode) } == 0 {
        return Err(());
    }
    Ok(())
}
