# scoop-fsearch (and scoop-manifest)

scoop-fsearch is a *fast* and *fancy* search tool for the [Scoop](https://scoop.sh/) package manager.

(See the end of this document for info about scoop-manifest.)


## Features

Compared to the built-in `scoop search` command, scoop-fsearch:

* has fancier output, with colors and package descriptions;
* is faster: with the *main* and *extras* buckets enabled, it takes around 0.1 seconds to run a warm search as opposed to around 2 seconds;
* searches package descriptions, in addition to package names and executable names;
* performs executable filename matching slightly differently;
* does not support searching remote buckets, only the local bucket cache.

![](https://gitlab.com/sjohannes/scoop-fsearch/uploads/0a0c217f6d918e61a78b4dd607b5002c/screenshot.png)

The output of scoop-fsearch is shown as soon as each package definition file is processed and may not be in any particular order.
However, if you sort the output, the search results will be grouped based on how they are found, for example whether the match is in the package name or in the description.
(In a GNU environment you may have to use `LC_ALL=C sort` to get sensible ordering.)


## Building

You need a Rust toolchain (rustc, libstd, cargo).
The latest stable version of the toolchain should work; if it doesn't, please file a bug report.
Then run:

```sh
cargo build --release
```

This creates `target/release/scoop-fsearch.exe`.


## Installation

Prebuilt binaries are available from the [Releases](https://gitlab.com/sjohannes/scoop-fsearch/-/releases) page.
Ideas on how to create an automatically-updating Scoop bucket for scoop-fsearch are welcome.

You can put `scoop-fsearch.exe` in any directory in your `$PATH` and run it with `scoop-fsearch`.

The release package includes an experimental PowerShell script that lets you run scoop-fsearch as `scoop fsearch` once added to Scoop's `shims` directory.
This integration is fragile and can cause an error message to show up in `scoop help` if at some point Scoop's internal help detection changes.
Please do not bother the Scoop developers if anything breaks due to this integration; file a bug report on scoop-fsearch instead.


## Contributing

If you contribute to this project, you agree to license your contribution under the AGPL-3.0-or-later license unless otherwise specified.


## Alternatives

[scoop-search](https://github.com/shilangyu/scoop-search) is a good alternative that is also fast but, unlike scoop-fsearch, aims to be compatible with the old `scoop search` output (which has changed since).


## Bonus: scoop-manifest

The scoop-fsearch package also comes with scoop-manifest, a very simple tool to display package manifests.
The two programs are fully independent and can be installed and run separately.
