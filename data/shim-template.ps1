# Usage: @usage@
# Summary: @summary@
# Help: @help@

# The following line is written very deliberately to work with `scoop help` and `scoop which`.
# It must start with exactly `$path` and must be the only line starting with it.
# When evaluated with no context, it must assign this file's path to $path.
# The last space-separated component must be a string containing the executable name.
$path = Join-Path ($script:MyInvocation.MyCommand.Path + "\..\..\..\..\..\shims") "scoop-@command@.ps1" # "scoop-@command@.exe"

& ($PSScriptRoot + "\scoop-@command@.exe") @args
