#!/bin/sh
set -ex

APPNAME=${APPNAME:=scoop-fsearch}
TARGET=${TARGET:=x86_64-pc-windows-gnu}

GIT=${GIT:=git}
SEVENZ=${SEVENZ:=7z}

git_desc="$($GIT describe --always --match 'v*.*.*')"
output_pkg="$APPNAME-$git_desc"
outdir="target/$output_pkg"

rm -rf "$outdir"
mkdir "$outdir"
install -p "target/$TARGET/release/"*.exe "$outdir"
install -p target/LICENSE.html "$outdir"
install -p target/*.ps1 "$outdir"

cd target
rm -f "$output_pkg.7z" "$output_pkg.zip"
$SEVENZ a -mx=9 "$output_pkg.7z" "$output_pkg"
$SEVENZ a -mx=9 -mfb=258 -mpass=15 "$output_pkg.zip" "$output_pkg"
wc -c "$output_pkg.7z" "$output_pkg.zip"
sha256sum "$output_pkg.7z" "$output_pkg.zip"
