#!/bin/sh
set -ex

shim_template="data/shim-template.ps1"

sed -E \
	-e 's|@command@|fsearch|g' \
	-e 's|@usage@|scoop fsearch <string>|' \
	-e 's|@summary@|Search for packages|' \
	-e 's|@help@|Search for a string in package names, descriptions, and executable names.\
# Only added/enabled buckets are searched.|' \
	"$shim_template" > "target/scoop-fsearch.ps1"

sed -E \
	-e 's|@command@|manifest|g' \
	-e 's|@usage@|scoop manifest <package>|' \
	-e 's|@summary@|Show package manifest|' \
	-e 's|@help@|Show package manifest.|' \
	"$shim_template" > "target/scoop-manifest.ps1"
