#!/bin/sh
set -ex

test -n "${CARGO_ABOUT_VER}"
test -n "${CARGO_ABOUT_HASH}"
test -n "${HOST}"

cargo_about_pkg=cargo-about-${CARGO_ABOUT_VER}-${HOST}

temp=$(mktemp -d)
if [ -z "$temp" ]; then exit 1; fi
cd "$temp"
time wget "https://github.com/EmbarkStudios/cargo-about/releases/download/${CARGO_ABOUT_VER}/${cargo_about_pkg}.tar.gz"
printf "${CARGO_ABOUT_HASH}  ${cargo_about_pkg}.tar.gz" | sha256sum -c
tar -xaf "${cargo_about_pkg}.tar.gz" "${cargo_about_pkg}/cargo-about"
mv "${cargo_about_pkg}/cargo-about" /usr/local/bin/cargo-about
rm -rf "$temp"
